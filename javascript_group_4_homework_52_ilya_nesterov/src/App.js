import React, { Component } from 'react';
import RandomNumber from './components/RandomNumber/RandomNumber';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
          <RandomNumber />
      </div>
    );
  }
}

export default App;
