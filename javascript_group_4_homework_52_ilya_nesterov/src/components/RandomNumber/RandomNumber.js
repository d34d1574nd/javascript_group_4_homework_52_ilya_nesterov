import React, { Component } from 'react';
import './RandomNumber.css';

class RandomNumber extends Component {
        state = {
            numbers: [],
        };

    handleClick = () => {
        const min = 5;
        const max = 36;
        const numbers = [...this.state.numbers];
        for (let i = 0; i < 5; i++) {
            const random = min + Math.floor(Math.random() * (max - min));
             if (numbers.includes(random)) {
                 console.log('number in game');
             } else {
                 numbers[i] = random;
             }
            numbers.sort(function (a, b) {
                return a - b;
            });
        }
        this.setState({numbers});
    };

    render () {
        return (
            <div className="game">
                <p>Добро пожаловать в игру 5 из 36. Поиграем?</p>
                <button onClick={this.handleClick.bind(this)}>New numbers</button>
                <div className="numbers">
                    <div className="number"><p>{this.state.numbers[0]}</p></div>
                    <div className="number"><p>{this.state.numbers[1]}</p></div>
                    <div className="number"><p>{this.state.numbers[2]}</p></div>
                    <div className="number"><p>{this.state.numbers[3]}</p></div>
                    <div className="number"><p>{this.state.numbers[4]}</p></div>
                </div>
            </div>
        )
    };
}

export default RandomNumber;
